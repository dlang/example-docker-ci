[![pipeline status](https://gitlab.com/dlang/example-docker-ci/badges/master/pipeline.svg)](https://gitlab.com/dlang/example-docker-ci/commits/master)
# Example Project to show how to use docker images with gitlab ci

To demonstrate and prove how to use the dlang docker images with gitlab CI.

## Dub with Compiler

... for everybody using dub.

### DUB with LDC

```yaml
  image: dlanguage/ldc:latest
  script:
    - dub
```

### DUB with DMD

```yaml
  image: dlanguage/dmd:latest
  script:
    - dub
```

## Pure Compilers

... for everybody using custom build scripts (e.g. make/cmake/...).

### LDC

```yaml
  image: dlanguage/ldc:latest
  script:
    - ldc2 -O5 -release -ofhello_world source/hello_world.d
```

### DMD

```yaml
  image: dlanguage/dmd:latest
  script:
    - dmd -O -release -of=hello_world source/hello_world.d
```

### GDC

```yaml
  image: dlanguage/gdc:latest
  script:
    - gdc -O -frelease -ohello_world source/hello_world.d
```
